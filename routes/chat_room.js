var express = require('express');
var router = express.Router();
var ChatRoom = require('../models/chat_room.js');

router.get('/', function(req, res) {
	sess = req.session;
	if (sess.users_un)
	{
		res.render('chat_room/index', {session_data:sess});
	}
	else
	{
		res.redirect('/login');
	}
});

router.get('/(:room_id)', function(req, res){
	sess = req.session;
	if (sess.users_un)
	{
		ChatRoom.findOne({'_id': req.params.room_id }, function(err, result){
			if (err)
			{
				res.send('page not found');
			}
			else
			{
				console.log(result);
				res.render('chat_room/detail', { room_id:req.params.room_id, room:result, session_data:sess });
			}
		});
	}
	else
	{
		res.redirect('/login');
	}
});

router.post('/add_chat_room', function(req, res) {
	var chatroom = new ChatRoom({
		name: req.param('name'),
		description: req.param('description'),
		room_types: req.param('room_types'),
		creator: req.param('creator'),
		members:[req.param('creator'),],
	});

	chatroom.save(function (err) {
		if (err) 
		{
			json_data = {
				status_code: 500,
				message: 'add chat room failed',
			}
		}
		else
		{
			json_data = {
				status_code: 200,
				message: 'add chat room success',
			}
		}
		
		res.json(json_data);
	});
});

router.post('/delete_chat_room', function(req, res){
	ChatRoom.remove({'_id':req.param('room_id')}, function(err){
		if (err) 
		{
			json_data = {
				status_code: 500,
				message: 'delete chat room failed',
			}
		}
		else
		{
			json_data = {
				status_code: 200,
				message: 'delete chat room success',
			}
		}
		
		res.json(json_data);
	});
});


router.post('/add_users', function(req, res) {
	console.log(req.param());
	ChatRoom.update({'_id':req.param('room_id')}, {$push: { members: req.param('users_name') } }, function (err) {
		if (err) 
		{
			json_data = {
				status_code: 500,
				message: 'add users to chat room failed',
			}
		}
		else
		{
			json_data = {
				status_code: 200,
				message: 'add users to chat room success',
			}
		}
		
		res.json(json_data);
	});
});

router.post('/kick_users', function(req, res) {
	console.log(req.param());
	ChatRoom.update({'_id':req.param('room_id')}, {$pull: { members: req.param('users_name') } }, function (err) {
		if (err) 
		{
			json_data = {
				status_code: 500,
				message: 'kick users from chat room failed',
			}
		}
		else
		{
			json_data = {
				status_code: 200,
				message: 'kick users from chat room success',
			}
		}
		
		res.json(json_data);
	});
});

module.exports = router;
