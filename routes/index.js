var express = require('express');
var router = express.Router();
var ChatRoom = require('../models/chat_room.js');
var User = require('../models/user.js');
var sess;

/* GET home page. */
router.get('/', function(req, res) {
	sess = req.session;
	if (sess.users_un)
	{
		res.render('home/index', {session_data:sess});
	}
	else
	{
		res.redirect('/login');
	}
});

router.get('/about', function(req, res) {
  res.render('home/about');
});

router.get('/credit', function(req, res) {
	res.render('home/credit');
});

router.get('/login', function(req, res ){
	res.render('auth/login');
});

router.post('/do_login', function(req, res ){
	sess = req.session;

	User.findOne({ 'username': req.param('users_un'), 'password': req.param('users_pw') }, function(err, result){
		if (err) res.redirect('/login');
		else if ( result == null || result == undefined ) res.redirect('/login');
		else 
		{
			sess.users_un = req.param('users_un');
			res.redirect('/');
		}
	});
});

router.post('/do_register', function(req, res ){
	sess = req.session;

	user = new User({ 
		username: req.param('reg_un'), 
		email: req.param('reg_email'), 
		password: req.param('reg_pw'), 
		about: '-',
		contacts:[],
	});

	user.save(function (err) {
      res.redirect('/login');
      
    });
});

router.get('/logout', function(req, res){
	req.session.destroy(function(err){
		if(err){
			console.log(err);
		}
		else
		{
			res.redirect('/login');
		}
	});
});

router.post('/get_chat_rooms', function(req, res){
	ChatRoom.find({'members':{'$in': [req.param('username')]}}, function(err, result){
		json_data = {
			status_code: 200,
			message: 'get chat room success',
			data: result
		}
		res.json(json_data);
	});
});

router.post('/get_chat_rooms_by_creator', function(req, res){
	ChatRoom.find({'creator':req.param('username')}, function(err, result){
		json_data = {
			status_code: 200,
			message: 'get chat room by creator success',
			data: result
		}
		res.json(json_data);
	});
});

router.post('/get_chat_room', function(req, res){
	ChatRoom.findOne({ '_id': req.param('room_id') }, function(err, result){
		json_data = {
			status_code: 200,
			message: 'get chat room by creator success',
			data: result
		}
		res.json(json_data);
	});
});

router.get('/create_chat_rooms', function(req, res){
	/*
		chat_room = new ChatRoom({
			creator:'ridwan',
			name: 'Inzpire',
			room_types: 'public',
			members: [
				'bot-1',
				'bot-2',
				'bot-3',
				'bot-4',
				'bot-5',
				'ridwan',
			]
		});

		chat_room.save(function (err) {
	      if (err) return console.error(err);
	    });

	    chat_room = new ChatRoom({
			creator:'bejo',
			name: 'POSS_UPI',
			room_types: 'public',
			members: [
				'bot-1',
				'bot-2',
				'bot-3',
				'bot-4',
				'bot-5',
				'ridwan',
				'bejo'
			]
		});

		chat_room.save(function (err) {
	      if (err) return console.error(err);
	    });

	    chat_room = new ChatRoom({
			creator:'fajar',
			name: 'OSTRIC',
			room_types: 'public',
			members: [
				'bot-1',
				'bot-2',
				'bot-3',
				'bot-4',
				'bot-5',
				'ridwan',
				'bejo',
				'fajar',
			]
		});

		chat_room.save(function (err) {
	      if (err) return console.error(err);
	    });
	*/
});

router.get('/create_users', function(req, res){
	/*user = new User({
		username:'ridwan',
		email: 'ridwan@gmail.com',
		password: 'ridwan',
		about: 'lorem ipsum sit dolor amet',
		contacts:[
			{address:'http://www.facebook.com/ridwan', type:'facebook'},
			{address:'http://www.twitter.com/ridwan', type:'twitter'},
			{address:'085712345678', type:'phone'},
		]
	});

	user.save(function (err) {
      if (err) return console.error(err);
    });

	user = new User({
		username:'bejo',
		email: 'bejo@gmail.com',
		password: 'bejo',
		about: 'lorem ipsum sit dolor amet',
		contacts:[
			{address:'http://www.twitter.com/bejo', type:'twitter'},
		]
	});

	user.save(function (err) {
      if (err) return console.error(err);
    });

    user = new User({
		username:'fajar',
		email: 'fajar@gmail.com',
		password: 'fajar',
		about: 'lorem ipsum sit dolor amet',
		contacts:[
			{address:'http://www.twitter.com/fajar', type:'twitter'},
		]
	});

	user.save(function (err) {
      if (err) return console.error(err);
    });

    user = new User({
		username:'rizky',
		email: 'rizky@gmail.com',
		password: 'rizky',
		about: 'lorem ipsum sit dolor amet',
		contacts:[
			{address:'http://www.twitter.com/rizky', type:'twitter'},
		]
	});

	user.save(function (err) {
      if (err) return console.error(err);
    });

    user = new User({
		username:'yogi',
		email: 'yogi@gmail.com',
		password: 'yogi',
		about: 'lorem ipsum sit dolor amet',
		contacts:[
			{address:'http://www.twitter.com/yogi', type:'twitter'},
		]
	});

	user.save(function (err) {
      if (err) return console.error(err);
    });
*/
    res.send('user berhasil dibuat..');
});


module.exports = router;
