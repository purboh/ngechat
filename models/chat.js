var mongoose = require('mongoose');

var chatSchema = new mongoose.Schema({
	sender: String,
	msgContent: String,
	chatRoomID: String,
	created_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Chat', chatSchema);