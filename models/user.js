var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	about:String,
	contacts: [],
	created_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('User', userSchema);