var mongoose = require('mongoose');

var chatRoomSchema = new mongoose.Schema({
	creator: String,
	name: String,
	room_types: String,
	description: String,
	members: [String],
	created_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('ChatRoom', chatRoomSchema);