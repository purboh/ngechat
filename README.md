#NgeChat

Apa yang digunakan di NgeChat:

* Node.js
* Express.js
* Twitter Bootstrap
* Mongoose
* MongoDB
* jQuery
* React.js
* SocketIO

Pengembangan apa yang akan dikerjakan selanjutnya:

* membuat halaman edit profile
* membuat halaman izin gabung di chatroom
* membuat halaman approval user yang ingin chatroom
* membuat deteksi ajax untuk mengetahui apakah user tersedia atau tidak
* berbagai notifikasi